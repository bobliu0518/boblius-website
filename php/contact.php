﻿<?php

if(!$_POST) exit;

function isEmail($email)
{
	return(preg_match("/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wang|wf|ws|xyz|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i",$email));
}

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");

$name     = $_POST['name'];
$email    = $_POST['email'];
$comments = $_POST['comments'];

if(trim($name) == '')
{
	echo '<div class="error_message">你必须输入你的名字。</div>';
	exit();
}
else if(trim($email) == '')
{
	echo '<div class="error_message">请输入一个正确的邮件地址。</div>';
	exit();
}
else if(!isEmail($email))
{
	echo '<div class="error_message">请输入一个正确的邮件地址。</div>';
	exit();
}

if(trim($comments) == '')
{
	echo '<div class="error_message">请输入你的留言。</div>';
	exit();
}

if(get_magic_quotes_gpc())
{
	$comments = stripslashes($comments);
}

$address = "13636381320@163.com";


$e_subject = '你收到了 ' . $name . '的留言。';

$e_body = "你收到了$name的留言。 他说了这些话：" . PHP_EOL . PHP_EOL;
$e_content = "\"$comments\"" . PHP_EOL . PHP_EOL;
$e_reply = "你可以联系 $name 通过这个邮件地址： $email 。";

$msg = wordwrap( $e_body . $e_content . $e_reply, 70 );

$headers = "来自: $email" . PHP_EOL;
$headers .= "回复: $email" . PHP_EOL;
$headers .= "MIME-Version: 1.0" . PHP_EOL;
$headers .= "Content-type: text/plain; charset=utf-8" . PHP_EOL;
$headers .= "Content-Transfer-Encoding: quoted-printable" . PHP_EOL;

if(mail($address, $e_subject, $msg, $headers))
{
	echo "<fieldset>";
	echo "<div id='success_page'>";
	echo "<h3>发送成功。</h3>";
	echo "<p>谢谢你，<strong>$name</strong>，你的信息也许对我有用。</p>";
	echo "</div>";
	echo "</fieldset>";

} else
{

	echo "额啊！！！出错了！！！";

}
?>
